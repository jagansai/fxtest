package com.driver;

import com.business.MarkupTableLoader;
import com.business.PnlCalculator;
import com.business.RatesLoader;
import com.business.TransactionLoader;
import com.core.CSVReader;
import com.model.MarkupTable;
import com.model.Rate;
import com.model.Transaction;
import com.util.Environment;
import com.util.Util;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.Currency;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

public class FxTest {

    static Stream<Transaction> getTransactions(String fileName) {
        try {
            final TransactionLoader transactionLoader = new TransactionLoader(new CSVReader(fileName, true));
            return transactionLoader.getTransactions();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return Stream.empty();
    }

    static Stream<Rate> getRates(String fileName) {
        try {
            final RatesLoader ratesLoader = new RatesLoader(new CSVReader(fileName, false));
            return ratesLoader.getRates();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return Stream.empty();
    }


    /*
     ** This is the entry point of the application.
     *  Arguments can be taken from command line or environment.
     *  Command line arguments have more precedence than environment variables.
     *  through the arguments.
     */
    public static void main(String[] args) throws Exception {

        loadArgs(args);

        Instant start = Instant.now();
        try {
            String transactionsFile = Environment.getString("TRANSACTIONS_FILE", "");
            String ratesFile = Environment.getString("RATES_FILE", "");
            String individualClientsMarkupFile = Environment.getString("INDIVIDUAL_CLIENTS", "");
            String corporateClientsMarkupFile = Environment.getString("CORPORATE_CLIENTS", "");
            String outputFile = Environment.getString("OUTPUT_FILE", "output.csv");
            String localCurrency = Environment.getString("LOCAL_CURRENCY", "SGD");

            Util.fileExists(transactionsFile, "TRANSACTIONS_FILE");
            Util.fileExists(ratesFile, "RATES_FILE");
            Util.fileExists(individualClientsMarkupFile, "INDIVIDUAL_CLIENTS");
            Util.fileExists(corporateClientsMarkupFile, "CORPORATE_CLIENTS");


            final Stream<Transaction> transactions = new TransactionLoader(new CSVReader(transactionsFile, true))
                    .getTransactions();
            final Stream<Rate> rates = new RatesLoader(new CSVReader(ratesFile, false)).getRates();
            final Stream<MarkupTable> individualClientsMarkup = new MarkupTableLoader(
                    new CSVReader(individualClientsMarkupFile, true)).getMarkupRows();
            final Stream<MarkupTable> corporateClientsMarkup = new MarkupTableLoader(
                    new CSVReader(corporateClientsMarkupFile, true)).getMarkupRows();

            final PnlCalculator pnlCalculator = new PnlCalculator(individualClientsMarkup, corporateClientsMarkup);
            System.out.println(String.format("PnlReport with local currency as %s", localCurrency));

            try (PrintWriter writer = new PrintWriter(
                    new FileWriter(String.format(outputFile, localCurrency), false))) {
                writer.println(pnlCalculator.pnlHeader(Currency.getInstance(localCurrency)));
                pnlCalculator.pnlRows(rates, transactions, Currency.getInstance(localCurrency))
                             .forEach(writer::println);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

            Instant finish = Instant.now();
            System.out.println(String.format("PnlCalculation completed. Took %d seconds",
                                             Duration.between(start, finish).getSeconds()));
        } catch (FileNotFoundException ex) {
            System.out.println(String.format("%s.Report not generated.", ex.getMessage()));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void usage() {
        Map<String, String> envAndDescriptionMap = new LinkedHashMap<>();
        envAndDescriptionMap.put("\n-t", "TRANSACTIONS_FILE");
        envAndDescriptionMap.put("\n-r", "RATES_FILE");
        envAndDescriptionMap.put("\n-i", "INDIVIDUAL_CLIENTS");
        envAndDescriptionMap.put("\n-c", "CORPORATE_CLIENTS");
        envAndDescriptionMap.put("\n-o", "OUTPUT_FILE");
        envAndDescriptionMap.put("\n-l", "LOCAL_CURRENCY\n");

        System.out.println(envAndDescriptionMap);
    }

    private static void loadArgs(String[] args) throws IOException {
        final boolean help = Stream.of(args).anyMatch(x -> x.equalsIgnoreCase("-h"));
        if (help) {
            usage();
            System.exit(0);
        }
        for (int index = 0; index < args.length; ) {
            if (index + 2 > args.length) break;
            switch (args[index].substring(0, 2)) {
                case "-t":
                    System.setProperty("TRANSACTIONS_FILE", args[index + 1]);
                    index += 2;
                    break;
                case "-r":
                    System.setProperty("RATES_FILE", args[index + 1]);
                    index += 2;
                    break;
                case "-i":
                    System.setProperty("INDIVIDUAL_CLIENTS", args[index + 1]);
                    index += 2;
                    break;
                case "-c":
                    System.setProperty("CORPORATE_CLIENTS", args[index + 1]);
                    index += 2;
                    break;
                case "-l":
                    System.setProperty("LOCAL_CURRENCY", args[index + 1]);
                    index += 2;
                    break;
                default:
                    index += 2;
            }
        }
    }
}
