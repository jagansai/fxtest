package com.util;

import java.util.Map;
import java.util.Optional;

public class Environment {
    static {
        for (Map.Entry<String, String> KeyValue : System.getenv().entrySet()) {
            System.setProperty(KeyValue.getKey(), KeyValue.getValue());
        }
    }

    public static boolean getBoolean(String name) {
        return Optional.ofNullable(System.getProperty(name)).map(x -> x.equalsIgnoreCase("true"))
                       .orElseGet(() -> {
                           System.out.println(String.format("%s not provided. Using false", name));
                           return false;
                       });
    }

    public static String getString(String name, String def) {
        return Optional.ofNullable(System.getProperty(name))
                       .orElseGet(() -> {
                           System.out.println(String.format("%s not provided. Using %s", name, def));
                           return def;
                       });
    }
}
