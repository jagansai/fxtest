package com.util;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Util {

    // Time format to parse the string with hh:mm.
    public static final String hourAndMinuteFormat = "k:m";

    public static LocalTime parseTime(String transactionTime, String pattern) {
        return LocalTime.parse(transactionTime, DateTimeFormatter.ofPattern(pattern));
    }

    public static boolean fileExists(String fileName, String message) throws FileNotFoundException {
        if (fileName.isEmpty() || !Files.exists(Paths.get(fileName), LinkOption.NOFOLLOW_LINKS))
            throw new FileNotFoundException(String.format("%s:%s not found", message, fileName));
        return true;
    }
}
