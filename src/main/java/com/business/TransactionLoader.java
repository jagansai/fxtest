package com.business;

import com.core.Resource;
import com.model.ClientType;
import com.model.Transaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.stream.Stream;

public class TransactionLoader {
    public enum Members {
        BaseCurrency,
        WantedCurrency,
        AmountInBaseCurrency,
        ClientType,
        TransactionTime;

        static List<String> getColumnNames() {
            return Arrays.asList("BaseCurrency", "WantedCurrency", "AmountInBaseCurrency", "ClientType", "TransactionTime");
        }
    }

    private Resource resource;

    public TransactionLoader(Resource resource) {
        this.resource = resource;
    }

    private List<String> getColumnNames() throws Exception {
        List<String> columnNames = resource.getColumnNames();
        return columnNames.isEmpty() ? Members.getColumnNames() : columnNames;
    }


    public Stream<Transaction> getTransactions() throws Exception {
        final List<String> columnNames = getColumnNames();
        return resource.getRow()
                       .map(r -> getTransaction(r, columnNames));
    }

    private Transaction getTransaction(List<String> row, List<String> columnNames) {
        Transaction transaction = new Transaction();
        int index = 0;
        for (final String columnName : columnNames) {
            switch (Members.valueOf(columnName)) {
                case BaseCurrency:
                    transaction.setBaseCurrency(Currency.getInstance(row.get(index)));
                    break;
                case WantedCurrency:
                    transaction.setWantedCurrency(Currency.getInstance(row.get(index)));
                    break;
                case AmountInBaseCurrency:
                    transaction.setAmountInBaseCurrency(new BigDecimal(row.get(index)).setScale(4, RoundingMode.HALF_DOWN));
                    break;
                case ClientType:
                    transaction.setClientType(ClientType.valueOf(row.get(index)));
                    break;
                case TransactionTime:
                    transaction.setTransactionTime(row.get(index));
                    break;
                default:
            }
            ++index;
        }
        return transaction;
    }
}
