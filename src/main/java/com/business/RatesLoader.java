package com.business;

import com.core.Resource;
import com.model.Rate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.stream.Stream;

public class RatesLoader {

    public enum Members {
        BaseCurrency,
        WantedCurrency,
        Rate,
        ValidUntil;

        static List<String> getColumnNames() {
            return Arrays.asList("BaseCurrency", "WantedCurrency", "Rate", "ValidUntil");
        }
    }

    private Resource resource;

    public RatesLoader() {
    }

    public RatesLoader(Resource resource) {
        this.resource = resource;
    }

    private List<String> getColumnNames() throws Exception {
        final List<String> columnNames = resource.getColumnNames();
        return columnNames.isEmpty() ? Members.getColumnNames() : columnNames;
    }

    public Stream<Rate> getRates() throws Exception {
        final List<String> columnNames = getColumnNames();
        return resource.getRow()
                       .map(r -> getRate(r, columnNames));
    }

    private Rate getRate(List<String> row, List<String> columnNames) {
        Rate rate = new Rate();
        int index = 0;
        for (final String columnName : columnNames) {
            switch (Members.valueOf(columnName)) {
                case BaseCurrency:
                    rate.setBaseCurrency(Currency.getInstance(row.get(index)));
                    break;
                case WantedCurrency:
                    rate.setWantedCurrency(Currency.getInstance(row.get(index)));
                    break;
                case Rate:
                    rate.setRate(new BigDecimal(row.get(index)).setScale(4, RoundingMode.HALF_DOWN));
                    break;
                case ValidUntil:
                    rate.setValidUntil(row.get(index));
                    break;
                default:
            }
            ++index;
        }
        return rate;
    }
}
