package com.business;

import com.core.Resource;
import com.model.MarkupTable;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class MarkupTableLoader {

    public enum Members {
        LowerLimit,
        UpperLimit,
        Markup;

        static List<String> getColumnNames() {
            return Arrays.asList("LowerLimit", "UpperLimit", "Markup");
        }
    }

    private Resource resource;

    public MarkupTableLoader(Resource resource) {
        this.resource = resource;
    }

    private List<String> getColumnNames() throws Exception {
        final List<String> columnNames = resource.getColumnNames();
        return columnNames.isEmpty() ? Members.getColumnNames() : columnNames;
    }

    public Stream<MarkupTable> getMarkupRows() throws Exception {
        final List<String> columnNames = getColumnNames();
        return this.resource.getRow()
                            .map(r -> getMarkup(r, columnNames));
    }

    private MarkupTable getMarkup(List<String> row, List<String> columnNames) {
        MarkupTable markupTable = new MarkupTable();
        int index = 0;
        for (final String columnName : columnNames) {
            switch (Members.valueOf(columnName)) {
                case LowerLimit:
                    markupTable.setLowerLimit(Double.parseDouble(row.get(index)));
                    break;
                case UpperLimit:
                    double upperlimit = row.get(index).isEmpty() ? Double.MAX_VALUE : Double.parseDouble(row.get(index));
                    markupTable.setUpperLimit(upperlimit);
                    break;
                case Markup:
                    markupTable.setMarkup(Double.parseDouble(row.get(index)));
                    break;
                default:
            }
            ++index;
        }
        return markupTable;
    }
}
