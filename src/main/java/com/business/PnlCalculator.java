package com.business;

import com.model.MarkupTable;
import com.model.Rate;
import com.model.Transaction;
import com.service.RateService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PnlCalculator {


    private static final Currency USD = Currency.getInstance("USD");


    private final static NumberFormat formatRates = new DecimalFormat("##.####");
    private final static NumberFormat formateProfit = new DecimalFormat("##.##");
    private final static int NUM_DECIMAL_PLACES = 4;
    private final static RoundingMode ROUND_MODE = RoundingMode.HALF_DOWN;
    private Currency localCurrency;
    private List<MarkupTable> individualClients;
    private List<MarkupTable> corporateClients;

    public PnlCalculator(Stream<MarkupTable> individualClients, Stream<MarkupTable> corporateClients) {
        this.individualClients = individualClients.collect(Collectors.toList());
        this.corporateClients = corporateClients.collect(Collectors.toList());
    }

    public String pnlHeader(Currency localCurrency) {
        this.localCurrency = localCurrency;
        return String
                .format("BaseCurrency,WantedCurrency,AmountInBaseCurrency,StandardRate,FinalRate,ProfitInWantedCurrency,ProfitIn%s",
                        this.localCurrency);
    }

    public Stream<String> pnlRows(Stream<Rate> rates, Stream<Transaction> transactions, Currency localCurrency) {
        this.localCurrency = localCurrency;
        final RateService rateService = new RateService(rates);

        // for each transaction in transaction stream, calculate the pnl and return as stream.
        return transactions.map(transaction -> {
            try {
                return getPnlRow(transaction, rateService);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return "";
        });
    }

    private BigDecimal getProfitInLocalCurrency(BigDecimal profitInWantedCurrency, Transaction transaction,
                                                RateService rateService) {
        if (!Objects.equals(transaction.getWantedCurrency(), localCurrency)) {
            final BigDecimal rateInLocalCurrency =
                    rateService.getRate(transaction.getWantedCurrency(), localCurrency,
                                        transaction.getTransactionTime())
                               .map(Rate::getRate)
                               .orElse(BigDecimal.ZERO);
            return profitInWantedCurrency.multiply(rateInLocalCurrency).setScale(NUM_DECIMAL_PLACES, ROUND_MODE);
        }
        return profitInWantedCurrency;
    }

    private String getPnlRow(Transaction transaction, RateService rateService) throws Exception {
        final BigDecimal standardRate =
                rateService.getRate(transaction.getBaseCurrency(), transaction.getWantedCurrency(),
                                    transaction.getTransactionTime())
                           .map(Rate::getRate)
                           .orElse(BigDecimal.ZERO);

        final double commissionRates = rateService.getCommissionRates(transaction, individualClients, corporateClients);
        final BigDecimal finalRate =
                standardRate.multiply(
                        BigDecimal.valueOf(1 - commissionRates / 100).setScale(NUM_DECIMAL_PLACES, ROUND_MODE));

        final BigDecimal profitInWantedCurrency =
                standardRate.subtract(finalRate)
                            .setScale(NUM_DECIMAL_PLACES, ROUND_MODE)
                            .multiply(transaction.getAmountInBaseCurrency())
                            .setScale(NUM_DECIMAL_PLACES, ROUND_MODE);

        BigDecimal profitInLocalCurrency = getProfitInLocalCurrency(profitInWantedCurrency, transaction,
                                                                    rateService);


        return String.format("%s,%s,%s,%s,%s,%s,%s", transaction.getBaseCurrency(),
                             transaction.getWantedCurrency(),
                             formateProfit.format(transaction.getAmountInBaseCurrency()),
                             formatRates.format(standardRate.doubleValue()),
                             formatRates.format(finalRate.doubleValue()),
                             formateProfit.format(profitInWantedCurrency.doubleValue()),
                             formateProfit.format(profitInLocalCurrency.doubleValue()));
    }
    /*
     *** utility method to convert double to BigDecimal with required decimal places and Rounding defaulted to CEILING.
     */
    private static BigDecimal parseDouble(BigDecimal value, NumberFormat numberFormat) {
        numberFormat.setRoundingMode(ROUND_MODE);
        return new BigDecimal(numberFormat.format(value));
    }
}
