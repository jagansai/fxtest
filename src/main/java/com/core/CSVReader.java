package com.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class CSVReader implements Resource {

    private String fileName;
    private boolean hasHeader;

    public CSVReader(String fileName, boolean hasHeader) {
        this.fileName = fileName;
        this.hasHeader = hasHeader;
    }

    @Override
    public List<String> getColumnNames() throws Exception {

        if (!hasHeader) return Collections.emptyList();

        if (!Files.exists(Paths.get(fileName)))
            throw new FileNotFoundException(String.format("%s is not found", fileName));

        try {
            return Arrays.asList(
                    Files.lines(Paths.get(fileName))
                         .findFirst()
                         .orElse("")
                         .split(",")
            );
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return Collections.emptyList();
    }

    @Override
    public Stream<List<String>> getRow() {
        int skipLines = this.hasHeader ? 1 : 0;
        if (Files.exists(Paths.get(fileName))) {
            try {
                return Files.lines(Paths.get(fileName))
                            .skip(skipLines) // skip the header.
                            .parallel()
                            .map(s -> Arrays.asList(s.split(",")));
            } catch (IOException ignore) {
            }
        }
        return Stream.empty();
    }
}
