package com.core;

import java.util.List;
import java.util.stream.Stream;

public interface Resource {

    public List<String> getColumnNames() throws Exception;

    public Stream<List<String>> getRow() throws Exception;
}
