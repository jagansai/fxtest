package com.model;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Currency;
import java.util.Objects;

import static com.util.Util.hourAndMinuteFormat;
import static com.util.Util.parseTime;

public class Rate {
    private Currency baseCurrency;
    private Currency wantedCurrency;
    private BigDecimal rate;
    private LocalTime validUntil;


    public Rate() {
    }

    public Rate(Currency baseCurrency, Currency wantedCurrency, BigDecimal rate, LocalTime validUntil) {
        this.baseCurrency = baseCurrency;
        this.wantedCurrency = wantedCurrency;
        this.rate = rate;
        this.validUntil = validUntil;
    }


    public Rate(Currency baseCurrency, Currency wantedCurrency, BigDecimal rate, String validUntil) {
        this.baseCurrency = baseCurrency;
        this.wantedCurrency = wantedCurrency;
        this.rate = rate;
        this.validUntil = parseTime(validUntil, hourAndMinuteFormat);
    }

    public Currency getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(Currency baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public Currency getWantedCurrency() {
        return wantedCurrency;
    }

    public void setWantedCurrency(Currency wantedCurrency) {
        this.wantedCurrency = wantedCurrency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public LocalTime getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalTime validUntil) {
        this.validUntil = validUntil;
    }

    public void setValidUntil(String validUntil) {
        this.validUntil = parseTime(validUntil, hourAndMinuteFormat);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Rate rate1 = (Rate) o;
        return rate1.rate.compareTo(rate) == 0 &&
                baseCurrency.equals(rate1.baseCurrency) &&
                wantedCurrency.equals(rate1.wantedCurrency) &&
                validUntil.equals(rate1.validUntil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(baseCurrency, wantedCurrency, rate, validUntil);
    }

    @Override
    public String toString() {
        return "Rate{" +
                "baseCurrency=" + baseCurrency +
                ", wantedCurrency=" + wantedCurrency +
                ", rate=" + rate +
                ", validUntil=" + validUntil +
                '}';
    }
}
