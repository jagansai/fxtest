package com.model;


/*
 *** This class holds the data for FX transactions.
 */

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Currency;
import java.util.Objects;

import static com.util.Util.hourAndMinuteFormat;
import static com.util.Util.parseTime;


public class Transaction {
    private Currency baseCurrency;
    private Currency wantedCurrency;
    private BigDecimal amountInBaseCurrency;
    private ClientType clientType;
    private LocalTime transactionTime;


    public Transaction() {
    }

    public Transaction(Currency baseCurrency, Currency wantedCurrency, BigDecimal amountInBaseCurrency, ClientType clientType, LocalTime transactionTime) {
        this.baseCurrency = baseCurrency;
        this.wantedCurrency = wantedCurrency;
        this.amountInBaseCurrency = amountInBaseCurrency;
        this.clientType = clientType;
        this.transactionTime = transactionTime;
    }

    public Transaction(Currency baseCurrency, Currency wantedCurrency, BigDecimal amountInBaseCurrency, ClientType clientType, String transactionTime) {
        this.baseCurrency = baseCurrency;
        this.wantedCurrency = wantedCurrency;
        this.amountInBaseCurrency = amountInBaseCurrency;
        this.clientType = clientType;
        this.transactionTime = parseTime(transactionTime, hourAndMinuteFormat);
    }


    public Currency getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(Currency baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public Currency getWantedCurrency() {
        return wantedCurrency;
    }

    public void setWantedCurrency(Currency wantedCurrency) {
        this.wantedCurrency = wantedCurrency;
    }

    public BigDecimal getAmountInBaseCurrency() {
        return amountInBaseCurrency;
    }

    public void setAmountInBaseCurrency(BigDecimal amountInBaseCurrency) {
        this.amountInBaseCurrency = amountInBaseCurrency;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    public LocalTime getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(LocalTime transactionTime) {
        this.transactionTime = transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = parseTime(transactionTime, hourAndMinuteFormat);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Transaction that = (Transaction) o;
        return that.amountInBaseCurrency.compareTo(amountInBaseCurrency) == 0 &&
                baseCurrency.equals(that.baseCurrency) &&
                wantedCurrency.equals(that.wantedCurrency) &&
                clientType == that.clientType &&
                transactionTime.equals(that.transactionTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(baseCurrency, wantedCurrency, amountInBaseCurrency, clientType, transactionTime);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "baseCurrency=" + baseCurrency +
                ", wantedCurrency=" + wantedCurrency +
                ", amountInBaseCurrency=" + amountInBaseCurrency +
                ", clientType=" + clientType +
                ", transactionTime=" + transactionTime +
                '}';
    }
}
