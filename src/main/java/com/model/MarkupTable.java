package com.model;

public class MarkupTable {
    double lowerLimit;
    double upperLimit;
    double markup;

    public MarkupTable() {
    }

    public MarkupTable(double lowerLimit, double upperLimit, double markup) {
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.markup = markup;
    }

    public double getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(double lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public double getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(double upperLimit) {
        this.upperLimit = upperLimit;
    }

    public double getMarkup() {
        return markup;
    }

    public void setMarkup(double markup) {
        this.markup = markup;
    }

    @Override
    public String toString() {
        return "MarkupTable{" +
                "lowerLimit=" + lowerLimit +
                ", upperLimit=" + upperLimit +
                ", markup=" + markup +
                '}';
    }
}
