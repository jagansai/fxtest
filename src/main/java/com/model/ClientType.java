package com.model;

public enum ClientType {
    Individual,
    Corporate
}
