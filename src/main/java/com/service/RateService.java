package com.service;

import com.model.MarkupTable;
import com.model.Rate;
import com.model.Transaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalTime;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RateService {
    private static final String rateGroupingFormat = "%s:%s";
    private static final Function<Rate, String> groupByBaseCcyAndWantedCcyMapper =
            rate -> String.format(rateGroupingFormat, rate.getBaseCurrency(), rate.getWantedCurrency());
    private static final Currency USD = Currency.getInstance("USD");
    private final static int NUM_DECIMAL_PLACES = 4;
    private final static RoundingMode ROUND_MODE = RoundingMode.HALF_DOWN;
    private final static long Corporate_Unit = 1_000_000;
    private Map<String, List<Rate>> groupByBaseCcyAndWantedCcy;

    public RateService(Stream<Rate> rates) {
        this.groupByBaseCcyAndWantedCcy = rates.collect(Collectors.groupingBy(groupByBaseCcyAndWantedCcyMapper));
    }

    public Optional<Rate> getRate(Currency baseCurrency, Currency wantedCurrency, LocalTime transactionTime) {
        return groupByBaseCcyAndWantedCcy
                .getOrDefault(String.format(rateGroupingFormat, baseCurrency, wantedCurrency), Collections.emptyList())
                .stream()
                .sorted(Comparator.comparing(Rate::getValidUntil))
                .filter(rate -> rate.getValidUntil().compareTo(transactionTime) >= 0)
                .findFirst();
    }

    private BigDecimal convertBaseAmtToUSD(Transaction transaction) {
        if (transaction.getBaseCurrency().equals(USD))
            return transaction.getAmountInBaseCurrency();

        Optional<Rate> rate = getRate(transaction.getBaseCurrency(), USD,
                                      transaction.getTransactionTime());
        return rate.map(r -> transaction.getAmountInBaseCurrency()
                                        .multiply(r.getRate()).setScale(NUM_DECIMAL_PLACES, ROUND_MODE))
                   .orElse(BigDecimal.ZERO);
    }

    public double getCommissionRates(Transaction transaction,
                                     List<MarkupTable> individualClients,
                                     List<MarkupTable> corporateClients) throws Exception {
        // Helper methods.
        BiPredicate<BigDecimal, MarkupTable> individualClientCondition =
                (usdAmount, m) -> usdAmount.compareTo(BigDecimal.valueOf(m.getLowerLimit())) > 0 &&
                        usdAmount.compareTo(BigDecimal.valueOf(m.getUpperLimit())) <= 0;

        BiPredicate<BigDecimal, MarkupTable> corporateClientCondition =
                (usdAmount, m) ->
                {
                    double lowerValue = Math.min(Double.MAX_VALUE, m.getLowerLimit() * Corporate_Unit);
                    double upperValue = Math.min(Double.MAX_VALUE, m.getUpperLimit() * Corporate_Unit);

                    return usdAmount.compareTo(BigDecimal.valueOf(lowerValue)) > 0 &&
                            usdAmount.compareTo(BigDecimal.valueOf(upperValue)) <= 0;
                };

        // End of Helper methods.

        switch (transaction.getClientType()) {
            case Individual:
                return calculateCommissionRates(individualClients, convertBaseAmtToUSD(transaction),
                                                individualClientCondition);
            case Corporate:
                return calculateCommissionRates(corporateClients, convertBaseAmtToUSD(transaction),
                                                corporateClientCondition);
            default:
                throw new Exception(String.format("Unknow ClientType:%s", transaction.getClientType()));
        }
    }

    private double calculateCommissionRates(List<MarkupTable> markupTable, BigDecimal amountInUSD,
                                            BiPredicate<BigDecimal, MarkupTable> condition) {

        return markupTable.stream().filter(m -> condition.test(amountInUSD, m))
                          .findFirst()
                          .map(m -> m.getMarkup() / 100)
                          .orElse(0d);
    }
}
