package com.model;

import com.business.RatesLoader;
import com.core.CSVReader;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RateTest {

    @Test
    public void testLoadingRateFromCsv() throws Exception {
        final RatesLoader ratesLoader = new RatesLoader(new CSVReader("src/test/resources/rates.csv", false));
        assertTrue(ratesLoader.getRates().count() > 0);
    }
}
