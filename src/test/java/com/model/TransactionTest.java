package com.model;

import com.business.TransactionLoader;
import com.core.CSVReader;
import org.junit.Test;

import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TransactionTest {

    @Test()
    public void testReadTransactionFromCsv() throws Exception {
        final TransactionLoader transactionLoader = new TransactionLoader(
                new CSVReader("src/test/resources/transactions.csv", true));
        assertTrue(transactionLoader.getTransactions().count() > 0);
    }

    @Test()
    public void testWithWrongHeader() {
        try {
            final TransactionLoader transactionLoader = new TransactionLoader(
                    new CSVReader("src/test/resources/transactionwithwrongheader.csv", true));
            final Stream<Transaction> transactions = transactionLoader.getTransactions();
            transactions.forEach(System.out::println);
        } catch (IllegalArgumentException ex) {
            assertTrue(ex instanceof IllegalArgumentException);
        } catch (Exception ex) {
            fail();
        }
    }
}
