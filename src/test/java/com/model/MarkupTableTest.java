package com.model;

import com.business.MarkupTableLoader;
import com.core.CSVReader;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MarkupTableTest {

    @Test
    public void loadMarkupTable() throws Exception {
        final MarkupTableLoader markupTableLoader = new MarkupTableLoader(new CSVReader("src/test/resources/corporateclientsmarkup.csv", true));
        assertTrue(markupTableLoader.getMarkupRows().count() > 0);
        markupTableLoader.getMarkupRows().forEach(System.out::println);
    }
}
