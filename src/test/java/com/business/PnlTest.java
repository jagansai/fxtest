package com.business;

import com.core.CSVReader;
import com.model.MarkupTable;
import com.model.Rate;
import com.model.Transaction;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Currency;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class PnlTest {

    private Stream<Transaction> getTransactions(String fileName) {
        try {
            return new TransactionLoader(new CSVReader(fileName, true)).getTransactions();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Stream.empty();
    }

    private Stream<Rate> getRates(String fileName) {
        try {
            return new RatesLoader(new CSVReader(fileName, false)).getRates();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Stream.empty();
    }

    private static void printPnReport(PnlCalculator pnlCalculator, Stream<Rate> rates, Stream<Transaction> transactions, PrintWriter writer, Currency localCurrency) throws
                                                                                                                                                                     IOException {
        writer.println(pnlCalculator.pnlHeader(localCurrency));
        pnlCalculator.pnlRows(rates, transactions, localCurrency)
                     .forEach(writer::println);
    }

    @Test
    public void testPnlReport() {
        Instant start = Instant.now();
        try {
            final Supplier<Stream<Transaction>> transactions = () -> getTransactions("src/test/resources/transactions.csv");
            final Supplier<Stream<Rate>> rates = () -> getRates("src/test/resources/rates.csv");
            final Stream<MarkupTable> individualClientsMarkup = new MarkupTableLoader(new CSVReader("src/test/resources/individualclientsmarkup.csv", true)).getMarkupRows();
            final Stream<MarkupTable> corporateClientsMarkup = new MarkupTableLoader(new CSVReader("src/test/resources/corporateclientsmarkup.csv", true)).getMarkupRows();

            final PnlCalculator pnlCalculator = new PnlCalculator(individualClientsMarkup, corporateClientsMarkup);

            for (String currency : Arrays.asList("SGD", "USD", "AUD", "CNY")) {
                System.out.println(String.format("PnlReport with local currency as %s", currency));
                try (PrintWriter writer = new PrintWriter(new FileWriter(String.format("output/output%s.csv", currency), false))) {
                    printPnReport(pnlCalculator, rates.get(), transactions.get(), writer, Currency.getInstance(currency));
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
            Instant finish = Instant.now();
            System.out.println(String.format("PnlCalculation completed. Took %d seconds", Duration.between(start, finish).getSeconds()));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
